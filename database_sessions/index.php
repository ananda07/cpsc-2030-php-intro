<?php
    session_start();

    function isLoggedIn() {
        return (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true);
    }

    $link = mysqli_connect( 'localhost', 'root', '27dec2016', 'cars_database' );
    mysqli_select_db( $link, 'cars_database' );

    if(isLoggedIn() && isset($_REQUEST['delete'])) {
        $deleteId = (int)$_REQUEST['delete'];
        $query = 'DELETE FROM cars WHERE id = '.$deleteId;
        mysqli_query( $link, $query );
        header("Location: index.php");
    }
    if (isset( $_REQUEST["make"] )) {
        $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
        $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
        $safe_year= mysqli_real_escape_string($link, $_REQUEST["year"]);
        $safe_mileage= mysqli_real_escape_string($link, $_REQUEST["mileage"]);

        if((!preg_match("/^[0-9A-Za-z ]+$/i", $safe_make)) || (!preg_match("/^[0-9A-Za-z ]+$/i", $safe_model)) || (!preg_match("/^[0-9]+$/", $safe_year)) || (!preg_match("/^[0-9]+$/", $safe_mileage))){
            echo "error!!";
        }
        else{
            $query = "INSERT INTO cars ( Make, Model, Year, Mileage ) VALUES ( '$safe_make', '$safe_model', '$safe_year', '$safe_mileage' )";

            if(!$query){
                print(mysqli_error($link));
            }
            else{
                if(!mysqli_query( $link, $query )) {
                    echo "Something went wrong!";
                }
            }
        }

    }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Cars - CPSC 2031</title>
  
  </head>
  <body class="container">
    <h1>Cars - CPSC 2030</h1>
    <p>
        <?php if(isLoggedIn()): ?>
            <a href="logout.php">Logout</a>
        <?php else: ?>
            <a href="login.php">Login</a>
        <?php endif ?>
    </p>


    

    <form class="needs-validation" novalidate method="POST" action="">
      <div class="row">
          <div class="col-md-6 offset-md-3">
            <label for="ID">ID Vin Number</label>
            <input type="text" class="form-control" id="ID" placeholder="" value="1HGBH41JXMN10908H33" required name="ID">
            <div class="invalid-feedback">
              Valid make is required.
             </div>
            </div>
          </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="Audi" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value=" BMW" required name="model">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
        </div>
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="mileage">Mileage</label>
              <input type="number" class="form-control" id="mileage" placeholder="" value="20000" required name="mileage">
              <div class="invalid-feedback">
                Valid make is required.
              </div>
            </div>
          <div class="col-md-6 mb-3">
            <label for="year">Year</label>
            <input type="number" class="form-control" id="year" placeholder="" value="2018" required name="year">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
        </div>
        <button id="add" class="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
    </form>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID VIN</th>
                <th scope="col">Make</th>
                <th scope="col">Model</th>
                <th scope="col">Mileage</th>
                <th scope="col">Year</th>
                <?php if(isLoggedIn()): ?>
                    <th scope="col">Action</th>
                <?php endif ?>
            </tr>
        </thead>
        <tbody>
            <?php


              $query = mysqli_query( $link, 'SELECT * FROM cars' );

              while( $record = mysqli_fetch_assoc( $query ) ) {
                $ID= $record["id"];
              	$make = $record['make'];
              	$model = $record['model'];
              	$year= $record['year'];
              	$mileage= $record['mileage'];
              	$deleteAction = isLoggedIn()? '<td><a href="index.php?delete='.$ID.'">Delete</a></td>' : '';
              	print "<tr><td>$ID</td><td>$make</td><td>$model</td><td>$year</td><td>$mileage</td>'.$deleteAction.'</tr>";
              }
            ?>


        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!--<script type="text/javascript" src="cars.js"></script>-->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="cars.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    
   
  </body>
</html>
